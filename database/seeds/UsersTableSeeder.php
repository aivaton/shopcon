<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create admin
        $admin = User::create([
            'first_name' => 'Admin',
            'last_name' => 'Adminsson',
            'email' => 'admin@shopcon.io',
            'password' => bcrypt('secret')
        ]);
        $admin->role = 'admin';
        $admin->save();

        // create user
        $user = User::create([
            'first_name' => 'User',
            'last_name' => 'Usersson',
            'email' => 'user@shopcon.io',
            'password' => bcrypt('secret')
        ]);
    }
}
