@extends('layouts.auth')

@section('content')
<div class="container-fluid">
    <div class="auth">
        <div class="inner">
            <img src="{{ asset('img/logotype.png') }}" style="filter:invert(1);" class="logotype">
            <h1>@lang('Forgot your password?')</h1>
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="form-group">
                    <label for="email">@lang('E-Mail Address')</label>
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-block btn-primary">
                        @lang('Send Password Reset Link')
                    </button>
                    <a class="btn btn-block btn-link" href="{{ route('login') }}">
                        @lang('Back To Login')
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
