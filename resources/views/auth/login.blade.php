@extends('layouts.auth')
@section('content')
<div class="container-fluid">
    <div class="auth">
        <div class="inner">
            <img src="{{ asset('img/logotype.png') }}" style="filter:invert(1);" class="logotype">
            <h1>@lang('Login To Your Account')</h1>
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="form-group">
                    <label for="email">@lang('E-Mail Address')</label>
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="email">@lang('Password')</label>
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-block btn-primary">
                        @lang('Login')
                    </button>
                    <a class="btn btn-block btn-link" href="{{ route('password.request') }}">
                        @lang('Forgot Your Password?')
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
