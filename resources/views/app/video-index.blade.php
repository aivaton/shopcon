@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card">
            <div class="table-responsive">
                <table class="table table-striped mb-0">
                    <thead>
                        <th>@lang('ID')</th>
                        <th>@lang('Title')</th>
                        <th>@lang('Duration')</th>
                        <th>@lang('Tags')</th>
                        <th class="text-center">@lang('Products')</th>
                        <th class="nw"></th>
                        <th class="nw"></th>
                    </thead>
                    <tbody>
                        @for ($i = 1; $i <= 5; $i++)
                            <tr>
                                <td class="text-muted">{{ str_random(8) }}</td>
                                <td>Världens bästa burgare S03E01</td>
                                <td>00:25:23</td>
                                <td>
                                    <span class="badge badge-primary">Hamburgare</span>
                                    <span class="badge badge-primary">McDonalds</span>
                                    <span class="badge badge-primary">Jureskog</span>
                                </td>
                                <td class="text-center lead">24</td>
                                <td>
                                    <a href="#" class="btn btn-sm btn-outline-primary"><i class="fas fa-video"></i> View</a>
                                </td>
                                <td>
                                    <a href="/app/videos/1" class="btn btn-sm btn-primary"><i class="fas fa-pen"></i> Edit</a>
                                </td>
                            </tr>
                        @endfor
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection