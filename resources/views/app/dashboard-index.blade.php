@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="alert alert-danger">
            You have one unpaid invoice! Please have a look at it ASAP.
            <a href="#" class="alert-link">View invoices &raquo;</a>
        </div>
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="card">
                    <div class="card-body pb-1">
                        <h5 class="card-title">How to get started</h5>
                        <div class="mb-3 dashboard-how-to">
                            <img src="http://fillmurray.com/160/90">
                            <p class="lead">Our step by step video.</p>
                            <a href="#" class="btn btn-secondary"><i class="fas fa-info-circle"></i> Read our FAQ</a>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Quick Stats</h5>
                        <canvas id="myChart" height="150"></canvas>
                        <table class="table table-striped table-sm mb-0">
                            <thead>
                                <th>Date</th>
                                <th>Views</th>
                                <th>Clicks</th>
                                <th>Sales</th>
                                <th class="text-right">Amount</th>
                            </thead>
                            <tbody>
                                @for ($i = 1; $i <= 4; $i++)
                                    <tr>
                                        <td>2018-12-04</td>
                                        <td>201 294</td>
                                        <td>3 204</td>
                                        <td>320</td>
                                        <td class="text-right">20 394 SEK</td>
                                    </tr>
                                    <tr>
                                        <td>2018-12-04</td>
                                        <td>201 294</td>
                                        <td>3 204</td>
                                        <td>320</td>
                                        <td class="text-right">20 394 SEK</td>
                                    </tr>
                                    <tr>
                                        <td>2018-12-04</td>
                                        <td>201 294</td>
                                        <td>3 204</td>
                                        <td>320</td>
                                        <td class="text-right">20 394 SEK</td>
                                    </tr>
                                    <tr>
                                        <td>2018-12-04</td>
                                        <td>201 294</td>
                                        <td>3 204</td>
                                        <td>320</td>
                                        <td class="text-right">20 394 SEK</td>
                                    </tr>
                                @endfor
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="card">
                    <div class="card-body pb-1">
                        <h5 class="card-title">Upload your first video</h5>
                        <div class="dashboard-video-empty mb-3">
                            <i class="fas fa-film"></i>
                            <p class="lead">You have no videos yet.</p>
                            <a href="#" class="btn btn-primary"><i class="fas fa-plus"></i> Upload Video</a>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="card">
                    <div class="card-body pb-1">
                        <h5 class="card-title">Latest videos</h5>
                        <div class="row">
                            @for ($i = 1; $i <= 4; $i++)
                                <div class="col-12 col-md-6 dashboard-video">
                                    <div class="card mb-3">
                                        <small class="date">2018-10-10</small>
                                        <img src="http://fillmurray.com/160/90" class="img-fluid card-img-top">
                                        <div class="card-body">
                                            <p><strong>Världens bästa burgare</strong></p>
                                            <table class="table table-sm">
                                                <tr>
                                                    <th>Views</th>
                                                    <td class="text-right">1000</td>
                                                </tr>
                                                <tr>
                                                    <th>Clicks</th>
                                                    <td class="text-right">1000</td>
                                                </tr>
                                                <tr>
                                                    <th>Sales</th>
                                                    <td class="text-right">1000</td>
                                                </tr>
                                                <tr>
                                                    <th>Amount</th>
                                                    <td class="text-right">1000 SEK</td>
                                                </tr>
                                            </table>
                                            <a href="#" class="btn btn-outline-primary btn-sm btn-block">Show more</a>
                                        </div>
                                    </div>
                                </div>
                            @endfor
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<a  href="#modal-pay" data-toggle="modal">Pay</a>
<div class="modal" tabindex="-1" role="dialog" id="modal-pay">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">buy buy buy</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <iframe src="http://wp.test/?action=shopcon-add-to-cart&products=27,28,29" style="width:100%;height:700px;border:none;"></iframe>
      </div>
    </div>
  </div>
</div>

    <script>
    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: [
                "2018-12-12",
                "2018-12-13",
                "2018-12-14",
            ],
            datasets: [{
                backgroundColor: 'rgba(238,234,244,0.5)',
                borderColor: 'rgba(238,234,244,1)',
                label: 'Views',
                data: [29000, 40999, 12322],
            }, {
                backgroundColor: 'rgba(221,213,234,0.5)',
                borderColor: 'rgba(221,213,234,1)',
                label: 'Clicks',
                data: [123, 432, 433]
            }, {
                backgroundColor: 'rgba(205,193,223,0.5)',
                borderColor: 'rgba(205,193,223,1)',
                label: 'Sales',
                data: [12, 32, 43]
            }, {
                backgroundColor: 'rgba(188,172,213,0.5)',
                borderColor: 'rgba(188,172,213,1)',
                label: 'Amount',
                data: [1294, 4234, 234]
            }]
        },
    });
    </script>
@endsection