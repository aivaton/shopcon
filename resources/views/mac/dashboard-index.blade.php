@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="alert alert-danger">
            You have one unpaid invoice! Please have a look at it ASAP.
            <a href="#" class="alert-link">View invoices &raquo;</a>
        </div>
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Quick Stats</h5>
                        <canvas id="myChart" height="150"></canvas>
                        <table class="table table-striped table-sm mb-0">
                            <thead>
                                <th>Date</th>
                                <th>Views</th>
                                <th>Clicks</th>
                                <th>Sales</th>
                                <th class="text-right">Amount</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>2018-12-04</td>
                                    <td>201 294</td>
                                    <td>3 204</td>
                                    <td>320</td>
                                    <td class="text-right">20 394 SEK</td>
                                </tr>
                                <tr>
                                    <td>2018-12-04</td>
                                    <td>201 294</td>
                                    <td>3 204</td>
                                    <td>320</td>
                                    <td class="text-right">20 394 SEK</td>
                                </tr>
                                <tr>
                                    <td>2018-12-04</td>
                                    <td>201 294</td>
                                    <td>3 204</td>
                                    <td>320</td>
                                    <td class="text-right">20 394 SEK</td>
                                </tr>
                                <tr>
                                    <td>2018-12-04</td>
                                    <td>201 294</td>
                                    <td>3 204</td>
                                    <td>320</td>
                                    <td class="text-right">20 394 SEK</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="card">
                    <div class="card-body pb-0">
                        <h5 class="card-title">News</h5>
                        <div class="media">
                            <img class="align-self-start mr-3" src="http://placehold.it/128x128" alt="Generic placeholder image">
                            <div class="media-body">
                                <h5 class="mt-0">We launch the new super cool AI</h5>
                                <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>
                                <p>Donec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                            </div>
                        </div>
                        <div class="media">
                            <img class="align-self-start mr-3" src="http://placehold.it/128x128" alt="Generic placeholder image">
                            <div class="media-body">
                                <h5 class="mt-0">We launch the new super cool AI</h5>
                                <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>
                                <p>Donec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: [
                "2018-12-12",
                "2018-12-13",
                "2018-12-14",
            ],
            datasets: [{
                backgroundColor: 'rgba(238,234,244,0.5)',
                borderColor: 'rgba(238,234,244,1)',
                label: 'Views',
                data: [29000, 40999, 12322],
            }, {
                backgroundColor: 'rgba(221,213,234,0.5)',
                borderColor: 'rgba(221,213,234,1)',
                label: 'Clicks',
                data: [123, 432, 433]
            }, {
                backgroundColor: 'rgba(205,193,223,0.5)',
                borderColor: 'rgba(205,193,223,1)',
                label: 'Sales',
                data: [12, 32, 43]
            }, {
                backgroundColor: 'rgba(188,172,213,0.5)',
                borderColor: 'rgba(188,172,213,1)',
                label: 'Amount',
                data: [1294, 4234, 234]
            }]
        },
    });
    </script>
@endsection