<?php

Route::get('/', function () {
    return view('welcome');
});

/**
 * Auth routes...
 */
Auth::routes();
Route::get('/auth/redirect', 'Auth\LoginController@redirectToCorrectAdmin');

/**
 * App routes...
 */
Route::group(['prefix' => 'app', 'middleware' => 'auth', 'namespace' => 'App'], function() {
    Route::get('/', 'DashboardController@index');
    Route::get('/videos', 'VideoController@index');
    Route::get('/videos/{id}', 'VideoController@show');
});

/**
 * Mac routes...
 */
Route::group(['prefix' => 'mac', 'middleware' => 'auth', 'namespace' => 'Mac'], function() {
    Route::get('/', 'DashboardController@index');
});