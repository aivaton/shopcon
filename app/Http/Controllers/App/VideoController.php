<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VideoController extends Controller
{
    public function __construct()
    {
        return \View::share([
            'tab' => 'video'
        ]);
    }

    public function index()
    {
        return view('app.video-index');
    }

    public function show(Request $request, $id)
    {
        return view('app.video-show');
    }
}
