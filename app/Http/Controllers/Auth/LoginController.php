<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/auth/redirect';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except([
            'logout',
            'redirectToCorrectAdmin'
        ]);
    }

    /**
     * Redirect to correct admin.
     */
    public function redirectToCorrectAdmin()
    {
        // User must be logged in.
        if (!Auth::check()) {
            return abort(404);
        }

        // return to "mac" or "app".
        return redirect(Auth::user()->role === 'admin' ? 'mac' : 'app');
    }
}
