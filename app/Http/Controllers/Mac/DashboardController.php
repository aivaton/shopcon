<?php

namespace App\Http\Controllers\Mac;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function __construct()
    {
        return \View::share([
            'tab' => 'home'
        ]);
    }

    public function index()
    {
        return view('mac.dashboard-index');
    }
}
